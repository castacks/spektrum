
#ifndef PARSER_H_
#define PARSER_H_

#include <vector>
#include <list>
#include <ros/ros.h>
#include <spektrum/PPM.h>

class PacketExtractor
{
public:	
  static const int PPM_SIZE=12;
  PacketExtractor();  
  bool addData(const char * data, unsigned int len);
  bool addData(unsigned char data);
  virtual void newPPMData(){}
 private:
  ros::NodeHandle node_handle;
  ros::Publisher ppm_data_pub;
  int m_ffcounter;
  int m_zerocounter;
  int m_datacount;
  char lastbyte;
  spektrum::PPM data;

  ros::Timer wdt;
  bool spektrumDead;
  void watchdogCallback(const ros::TimerEvent& event);
};

#endif /* PARSER_H_ */
