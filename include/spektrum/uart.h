/*
 * Copyright 2011 Virginia Polytechnic Institute and State University.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 		* Redistributions of source code must retain the above copyright
 * 		  notice, this list of conditions and the following disclaimer.
 * 		* Redistributions in binary form must reproduce the above copyright
 * 		  notice, this list of conditions and the following disclaimer in the
 * 		  documentation and/or other materials provided with the distribution.
 * 		* Neither the name of the <organization> nor the
 * 		  names of its contributors may be used to endorse or promote products
 * 		  derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 *
 * UART.h
 *
 * The purpose of this class is to abstract a serial port to a universal
 * asynchronous receiver/transmitter (UART) to a class which provides
 * synchronous write operations, and asynchronous reads.  All reads are handled
 * by new thread, which executes a "process" function of a UARTListener
 * object.  An arbitrary number of UARTListeners may be added to the UART. Any
 * read() errors are printed to std::cerr.
 *
 * This UART class is dependent on the cross-platform boost/asio library.
 * As such, it is portable to both Linux and Windows.  Development was 
 * performed using boost version 1.44.0.
 *
 *  Created on: April 13, 2011
 *      Author: Kevin Stefanik (stefanik08@gmail.com)
 */

#ifndef UART_H_
#define UART_H_

#include "boost/asio/serial_port.hpp"
#include "boost/asio/placeholders.hpp"
#include "boost/bind/bind.hpp"
#include "boost/thread.hpp"
#include <list>
#include <vector>

class UART
{
public:
	/**
	 * Constructor - attempts to open the given serial port, if specified.
	 * Will start second thread if successful.
	 * @param dev is the serial port device name.
	 * 		e.g. 	Windows: "COM1"
	 * 				Linux: "/dev/ttyS0"
	 * @param baud is the baudrate.  Some valid baud rates are (depending on
	 * 		your port type):
	 * 		300, 600, 1200, 1800, 2400, 4800, 7200, 9600, 14400, 19200, 38400,
	 * 		57600, 115200, 230400, 460800, 921600
	 * @param pcktSize - the value sent to the "read" command.  This acts as a
	 * 		maximum value for the length of data given to UARTListener::process
	 */
  UART(const std::string device, unsigned int baud = 9600, int pcktSize = 256);
	/**
	 * Destructor - ends asynchronous read() thread, and closes the port.
	 *		Note that this will always cancel the last read, so an error
	 *		will print.
	 */
	~UART();
	/**
	 * useDev(char*, unsigned int) - This allows the user to change the port
	 * 		on-the-fly.  It cancels any current read operations in process.
	 * @param dev is the serial port device name.
	 * 		e.g. 	Windows: "COM1"
	 * 				Linux: "/dev/ttyS0"
	 * @param baud is the baudrate.  Some valid baud rates are (depending on
	 * 		your port type):
	 * 		300, 600, 1200, 1800, 2400, 4800, 7200, 9600, 14400, 19200, 38400,
	 * 		57600, 115200, 230400, 460800, 921600
	 */
  bool useDev(const std::string dev, unsigned int baud = 9600);

	/**
	 * isOpen()
	 * @return whether the port has been successfully opened
	 */
	bool isOpen();

	/**
	 * write(char*, unsigned int) - writes the byte array to the port.  This
	 * 		does not return until all bytes are written.
	 * @param data is the byte array
	 * @param len is the number of bytes to write.  If unspecified, or -1,
	 * 		it will use the value returned by strlen("") in <cstring>
	 * @return true if successful (port is open), false otherwise
	 */
	bool write(const char* data, int len = -1);

	/**
	 * write(char*, unsigned int) - writes the byte array to the port.  This
	 * 		does not return until all bytes are written.
	 * @param data is the byte array
	 * @return true if successful (port is open), false otherwise
	 */
	bool write(const std::vector<char> &data);
        
  void variableWrite(const std::vector<char> &data);
  void variableWrite(const char *data, int len);
	/**
	 * UARTListener is for user implementation, for use by a UART object.
	 */
	class UARTListener
	{
	public:
		/**
		 * process(char*, int) - This function is called every time up to
		 * 		pcktSize (specified in constructor) bytes are read.  Note that
		 * 		pcktSize is not settable to avoid synchronous issues.
		 * @param data is the byte array to process
		 * @param len is the length of the byte array
		 */
		virtual void process(const char* data, int len) = 0;
	};

	/**
	 * addUARTList(UARTListener*) - This function adds a user's UARTListener
	 * 		pointer to this UART.  Note that each pointer MUST be valid until
	 * 		the destructor of UART is called.  Multiple pointers may be added.
	 */
	void addUARTList(UARTListener* list);

	/**
	 * removeUARTList(UARTListener*) - This function removes a user's
	 * 		UARTListener pointer to this UART.
	 */
	void removeUARTList(UARTListener* list);

	void close();

private:
	char * data;
	int pcktSize;
	bool go;
	std::list<UARTListener*> lists;
	boost::asio::io_service service;
	boost::asio::serial_port* port;
	boost::thread* t;

	void start();
	void stop();
	void readHandler(const boost::system::error_code& error,
			std::size_t bytes_transferred);
	void run();
};

#endif /* UART_H_ */
