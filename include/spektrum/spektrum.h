#ifndef SPEKTRUM_H_
#define SPEKTRUM_H_

#include "spektrum/uart.h"
#include "spektrum/parser.h"

#include <vector>

namespace spektrum
{



  




class SerialSpektrum : public PacketExtractor
{
public:
  SerialSpektrum(const std::string dev);
	~SerialSpektrum();
	bool isGood();
	void close()
	{
		uart->close();
	}
private:
	UART * uart;
	class SpektrumList : public UART::UARTListener
	{
	public:
	  SpektrumList(PacketExtractor* p)
	    {
	      pex = p;
	    }
	  /**
	   * process(char*, int) - This function is called every time up to
	   * 		pcktSize (specified in constructor) bytes are read.  Note that
	   * 		pcktSize is not settable to avoid synchronous issues.
	   * @param data is the byte array to process
	   * @param len is the length of the byte array
	   */
	  virtual void process(const char* data, int len)
	  {
	    pex->addData(data, len);
	  }
	private:
	  PacketExtractor* pex;
	};
	SpektrumList * spektrumlist;
};


}

#endif 
