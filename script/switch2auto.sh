#!/bin/bash

rostopic pub --once /spektrum/status spektrum/MikrokopterStatus  '{header: auto, isAutonomous: 1}'
