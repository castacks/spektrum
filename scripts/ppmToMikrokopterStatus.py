#!/usr/bin/env python
import roslib; roslib.load_manifest('spektrum')
import rospy
from spektrum.msg import PPM, MikrokopterStatus


'''Converts a PPM signal to the mikrokopter status'''

class PPMFilter(object):
    def __init__(self):
        self.sp_pub = rospy.Publisher('/spektrum/status',MikrokopterStatus)
        rospy.Subscriber("/spektrum/PPM", PPM, self.ppm_callback)

    def ppm_callback(self,ppmdata):
        msg              = MikrokopterStatus()
        msg.header       = ppmdata.header
        msg.isAutonomous = ppmdata.channels[4]>16000
        if ppmdata.channels[5] < -16000 :
            msg.gpsmode      = MikrokopterStatus.GPS_NOGPS
        elif ppmdata.channels[5] >16000 :
            msg.gpsmode      = MikrokopterStatus.GPS_COMEHOME
        else:
            msg.gpsmode      = MikrokopterStatus.GPS_GPSHOLD
        if ppmdata.channels[6] < -16000:
            msg.altmode      = MikrokopterStatus.ALT_MANUAL
        else:
            msg.altmode      = MikrokopterStatus.ALT_CONTROL
        scalefactor = 32280.0
        msg.throttle = ppmdata.channels[0] / scalefactor
        msg.pitch    = ppmdata.channels[1] / scalefactor
        msg.roll     = ppmdata.channels[2] / scalefactor
        msg.yaw      = ppmdata.channels[3] / scalefactor
        msg.extra    = ppmdata.channels[7] / scalefactor
        
        msg.propsOn = throttle > -0.5
        
        self.sp_pub.publish(msg)
        
def listener():
    rospy.init_node('ppmToMikrokopterStatus')
    pf = PPMFilter()
    rospy.spin()

if __name__ == '__main__':
    listener()
