/*
* Copyright (c) 2016 Carnegie Mellon University, Author <basti@andrew.cmu.edu>
*
* For License information please see the LICENSE file in the root directory.
*
*/


#include <spektrum/spektrum.h>
#include <iostream>
#include <ros/ros.h>

using std::cout;
using std::endl;
using std::hex;
using std::dec;

using namespace spektrum;




SerialSpektrum::SerialSpektrum(const std::string dev) : PacketExtractor()
{
  
  uart = new UART(dev, 115200, 18);
  spektrumlist = new SpektrumList(this);
  uart->addUARTList(spektrumlist);
}
SerialSpektrum::~SerialSpektrum()
{
	delete uart;
	delete spektrumlist;
}
bool SerialSpektrum::isGood()
{
	return uart->isOpen();
}
