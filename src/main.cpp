/*
* Copyright (c) 2016 Carnegie Mellon University, Author <basti@andrew.cmu.edu>
*
* For License information please see the LICENSE file in the root directory.
*
*/


#include <iostream>


#include <spektrum/PPM.h>
#include <ros/ros.h>
#include <spektrum/spektrum.h>


using namespace std;


using namespace spektrum;




int main(int argc, char **argv)
{
	//Now start ROS
	ros::init(argc, argv, "spektrum");
	ros::NodeHandle n;
	std::string portname;
	if(!n.getParam("/spektrum/port",portname))
	  {
	    ROS_ERROR_STREAM("Port not defined");
	    return -1;
	  }
	
	SerialSpektrum gx(portname);
	
	if (!gx.isGood())
	{
	  ROS_ERROR_STREAM("port not opened!");
	  return -1;
	}
	ROS_INFO_STREAM("successfully opened port!");
	//Spin and wait
	ros::spin();
	return 0;
}
