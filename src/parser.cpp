/*
* Copyright (c) 2016 Carnegie Mellon University, Author <basti@andrew.cmu.edu>
*
* For License information please see the LICENSE file in the root directory.
*
*/


#include "spektrum/parser.h"
#include <iostream>
#include <cmath>
#include <iomanip>
#include <ios>
using namespace std;
 


PacketExtractor::PacketExtractor():
  m_ffcounter(0),
  m_zerocounter(0),
  m_datacount(0),
  lastbyte(0)
{
  ppm_data_pub = node_handle.advertise<spektrum::PPM>("/spektrum/PPM", 100); 
	wdt = node_handle.createTimer(ros::Duration(1.0), &PacketExtractor::watchdogCallback, this);
  spektrumDead = false; 
}


bool PacketExtractor::addData(const char * data, unsigned int len)
{
	bool result = false;
	for (unsigned int i = 0; i < len; i++)
	{
		if (addData(data[i]))
			result = true;
	}
	return result;
}


string convBase(unsigned long v, long base)
{
	string digits = "0123456789abcdef";
	string result;
	if((base < 2) || (base > 16)) {
		result = "Error: base out of range.";
	}
	else {
		do {
			result = digits[v % base] + result;
			v /= base;
		}
		while(v);
	}
	return result;
}





bool PacketExtractor::addData(unsigned char c)
{
  //  std::cout<<" "<< convBase(c,16)<<" ";
  
  if(m_ffcounter<4)
    if( c == 0xFF)//-1)/x1/0xFF)
      {
	m_ffcounter++;
	m_zerocounter = 0;
	m_datacount = 0;
	//std::cout<<"\n";
      } 
    else
      {
	m_ffcounter = 0;
	m_zerocounter = 0;
	m_datacount = 0;
      }
  else
    {
      if(m_zerocounter<1)
	{	
	  if( c==0x00)
	    {
	      m_datacount = 0;
	      m_zerocounter++;
	      
	    }
	  else if(c!=0xFF)
	    {
	      m_ffcounter = 0;
	      m_zerocounter = 0;
	      m_datacount = 0;
	    }
	}
    }

     
     
     if(m_ffcounter>=3 && m_zerocounter >=1)
       {
	 if(m_datacount == 0)
	   {
	     data.header.stamp = ros::Time::now();
	     data.header.frame_id = "spektrum";
	   }
	 if(m_datacount %2 == 1)
	   {
	     unsigned short   Channel = ((unsigned short)(lastbyte) << 8) | c;        
	     short signal = Channel & 0x7ff;
	     signal -= 0x400; 
	     signal = signal * 42;   
	     short index = (lastbyte >> 3) & 0x0f;
	     if(index>=0 && index < PPM_SIZE)
	       {
		 data.channels[index] = signal;
	       } 
	     //std::cout<<" channel " <<convBase(Channel,2) << " signal "<<convBase(signal,2)<<" "<<signal<<" index "<<index<<std::endl; 
	   }
	 if(m_datacount>26)
	   {
	     m_ffcounter = 0;
	     m_zerocounter = 0;
	     ppm_data_pub.publish(data);
       spektrumDead = false;
	     //std::cout<<std::endl;
	   }
	 m_datacount++;
       }
     

     // std::cout<<fixed<<"ff"<<m_ffcounter<<"zero"<<m_zerocounter<<std::endl;
 lastbyte = c;
 return false;
}

void PacketExtractor::watchdogCallback(const ros::TimerEvent& event)
{
  if (spektrumDead)
  {
    ROS_FATAL("Spektrum PPM driver is DOWN!!");
  }
  else
    spektrumDead = true;

  return;
}
